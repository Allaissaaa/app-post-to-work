// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { BASE_URL } from '../../config';

import Echo from '../../helpers/echo';

// Define a service using a base URL and expected endpoints
export const conversationsApi = createApi({
  reducerPath: 'conversationsApi',
  baseQuery: fetchBaseQuery({ 
    baseUrl: BASE_URL,
    prepareHeaders: (headers, { getState }) => {
      const token = getState()?.token?.value || '';

      // If we have a token set in state, let's assume that we should be passing it.
      if (token) {
        headers.set('Authorization', `Bearer ${token}`)
      }

      headers.set('Accept', 'application/json');

      return headers
    },
  }),
  keepUnusedDataFor: 1,
  tagTypes: ['Conversation', 'Message'],
  endpoints: (builder) => ({    
    getConversations: builder.query({
      query: ({ query }) => `/inbox?q=${query}`,
      providesTags: ['Conversation'],
    }),
    addConversation: builder.mutation({
      query: (body) => ({
        url: `/inbox/create`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Conversation'],
    }),
    getConversation: builder.query({
      query: ({ id }) => `/inbox/show/${id}`,
      providesTags: ['Conversation'],
    }),
    getMessages: builder.query({
      query: ({ id }) => `/inbox/show/${id}?messages=true`,
      providesTags: ['Message'],
      async onCacheEntryAdded(
        { id, channel, token },
        { cacheEntryRemoved, updateCachedData, cacheDataLoaded }
      ) {
        try {
          await cacheDataLoaded;
          Echo?.channel(channel)
          .listen('MessageEvent', message => {
            updateCachedData((currentCacheData) => {
              currentCacheData.push(message.data);
            });
          });
          await cacheEntryRemoved;
          Echo?.leaveChannel(channel);
          Echo?.leave(channel);
        } catch (error) {
          console.log(error);
        }
      },
    }),
    addMessage: builder.mutation({
      query: (body) => ({
        url: `/inbox/send/${body.id}`,
        method: 'POST',
        body: body.data,
      }),
      invalidatesTags: ['Conversation', 'Message'],
    }),
  }),
})

export const {
  useGetConversationsQuery, 
  useGetConversationQuery,
  useGetMessagesQuery, 
  useAddConversationMutation, 
  useAddMessageMutation,
  util
} = conversationsApi
