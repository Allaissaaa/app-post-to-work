// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { BASE_URL } from '../../config';

// Define a service using a base URL and expected endpoints
export const registerApi = createApi({
  reducerPath: 'registerApi',
  baseQuery: fetchBaseQuery({ 
    baseUrl: BASE_URL,
    prepareHeaders: (headers, { getState }) => {
      const token = getState()?.token?.value || '';

      // If we have a token set in state, let's assume that we should be passing it.
      if (token) {
        headers.set('Authorization', `Bearer ${token}`);
      }

      console.log('token', token);

      headers.set('Accept', 'application/json');

      return headers;
    },
  }),
  tagTypes: [],
  endpoints: (builder) => ({
    register: builder.mutation({
      query: (body) => ({
        url: '/register',
        method: 'POST',
        body,
      }),
    }),
    registerWithOtp: builder.mutation({
      query: (body) => ({
        url: '/register/otp',
        method: 'POST',
        body,
      }),
    }),
    verifyContactNoSendOtp: builder.mutation({
      query: (body) => ({
        url: '/register/verify/send-otp',
        method: 'POST',
        body,
      }),
    }),
    verifyContactNo: builder.mutation({
      query: (body) => ({
        url: '/register/verify',
        method: 'POST',
        body,
      }),
    }),
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { 
  useRegisterMutation,
  useRegisterWithOtpMutation,
  useVerifyContactNoSendOtpMutation,
  useVerifyContactNoMutation,
} = registerApi