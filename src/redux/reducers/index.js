import { combineReducers } from 'redux';

// Reducers
import userReducer from './user';
import tokenReducer from './token';
import notificationsReducer from './notifications';

// Services
import { loginApi } from '../services/login';
import { registerApi } from '../services/register';
import { profileApi } from '../services/profile';
import { conversationsApi } from '../services/conversations';
import { fileApi } from '../services/files';
import { notificationApi } from '../services/notifications';
import { jobPostApi } from '../services/job-posts';
import { applicationApi } from '../services/applications';

// Combine Reducers
const appReducer = combineReducers({
  user: userReducer,
  token: tokenReducer,
  notifications: notificationsReducer,
  [profileApi.reducerPath]: profileApi.reducer,
  [loginApi.reducerPath]: loginApi.reducer,
  [registerApi.reducerPath]: registerApi.reducer,
  [conversationsApi.reducerPath]: conversationsApi.reducer,
  [fileApi.reducerPath]: fileApi.reducer,
  [notificationApi.reducerPath]: notificationApi.reducer,
  [jobPostApi.reducerPath]: jobPostApi.reducer,
  [applicationApi.reducerPath]: applicationApi.reducer,
});

// Middlewares
export const servicesMiddleware = [
  loginApi.middleware,
  registerApi.middleware,
  profileApi.middleware,
  conversationsApi.middleware,
  fileApi.middleware,
  notificationApi.middleware,
  jobPostApi.middleware,
  applicationApi.middleware,
];

export default appReducer;