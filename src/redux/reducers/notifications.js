import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from '../../helpers/axios';

export const fetchNotifications = createAsyncThunk(
  'notifications',
  async () => {
    const response = await axios.get('/notifications/count');
    return response.data;
  }
)

export const notificationsSlice = createSlice({
  name: 'notifications',
  initialState: {
    value: 0
  },
  reducers: {
    storeNotifications: (state, action) => {
      state.value = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchNotifications.fulfilled, (state, action) => {
      state.value = action.payload;
    })
  },
})

export const { storeNotifications } = notificationsSlice.actions;
export const selectNotifications = state => state.notifications.value;

export default notificationsSlice.reducer;