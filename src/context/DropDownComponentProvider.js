import React, { useRef } from 'react';
import DropdownAlert from 'react-native-dropdownalert';

const DropDownContext = React.createContext();

export const DropDownComponentProvider = ({ children }) => {
  const dropdownAlert = useRef();

  return (
    <DropDownContext.Provider
      value={{
        dropdownAlert,
      }}
    >
      {children}
      <DropdownAlert ref={dropdownAlert} />
    </DropDownContext.Provider>
  );
};

export const useDropDown = () => React.useContext(DropDownContext);