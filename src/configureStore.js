import thunk from 'redux-thunk';
import { configureStore } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import appReducer, { servicesMiddleware } from './redux/reducers';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['type']
};

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    state = undefined;
  }

  return appReducer(state, action);
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  middleware: [ thunk, ...servicesMiddleware ],
});

let persistor = persistStore(store);

export {
  store,
  persistor,
};