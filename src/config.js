import { Platform } from 'react-native';

const config = Platform.select({
  ios: {
    base_url: 'https://posttowork.online/api',
    pusher_auth_endpoint: 'https://posttowork.online/api/broadcasting/auth',
    websocket_host: 'posttowork.online',
    websocket_port: 443,
  },
  android: {
    base_url: 'https://posttowork.online/api',
    pusher_auth_endpoint: 'https://posttowork.online/api/broadcasting/auth',
    websocket_host: 'posttowork.online',
    websocket_port: 443,
  },
});

export const BASE_URL = config.base_url;
export const PUSHER_AUTH_ENDPOINT = config.pusher_auth_endpoint;
export const PUSHER_KEY = 'wzQK8vsdwMlyogC8AJN19tSI52gDDNcJ091mb9SnN7ip8wWPkd';
export const WEBSOCKET_HOST = config.websocket_host;
export const WEBSOCKET_PORT = config.websocket_port;