import React, { useState } from 'react';
import { View, Text, TouchableOpacity  } from 'react-native';
import Collapsible from 'react-native-collapsible';

function CollapsibleView({ button, style, children, collapsed = true }) {
  const [isCollapsed, setIsCollapsed] = useState(collapsed);

  return (
    <>
      {button(setIsCollapsed, isCollapsed)}
      <Collapsible collapsed={isCollapsed}>
        <View style={style}>
          {children(setIsCollapsed, isCollapsed)}
        </View>
      </Collapsible>
    </>
  )
}

export default CollapsibleView;