import React, { useState } from 'react';
import { Pressable, Text } from 'native-base';

function SeeMore({ string = '', count = 30, onPress = null }) {
  const [truncate, setTruncate] = useState(true);

  if (string.length < count) return <Text>{string}</Text> ;

  return (
    <Pressable
      onPress={() => {
        if (onPress) {
          onClick();
        } else {
          setTruncate(prevState => !prevState);
        }
      }}
    >
      <Text>
        {truncate ? `${string.substring(0, count)} ` : `${string} `}
        See {truncate ? 'More' : 'Less'}...
      </Text>
    </Pressable>

  );
}

export default SeeMore;