import React, { useState } from 'react';
import { Button, Modal, HStack, Heading, Center } from 'native-base';
import { useDropDown } from '../context/DropDownComponentProvider';
import { useAuth } from '../helpers/auth';

function LogOut({ children }) {
  const auth = useAuth();
  const { dropdownAlert } = useDropDown();
  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  return (
    <>
      {children(setModalVisible)}
      <Modal 
        isOpen={modalVisible} 
        onClose={() => setModalVisible(false)} 
        width="100%"
      >
        <Modal.Content
          width="90%"
          p="3"
        >
          <Modal.Body>
            <Center>
              <Heading mb="2" textAlign="center">Are you sure you want to logout?</Heading>
            </Center>
            <HStack justifyContent="center" mt="3">
              <Button
                p="2"
                borderRadius="50"
                size="lg"
                w="40%"
                bg="#eeeeee"
                _text={{ color: '#333333'}}
                isDisabled={loading}
                onPress={() => setModalVisible(false)}
                mr="2"
              >
                Cancel
              </Button>
              <Button
                p="2"
                borderRadius="50"
                size="lg"
                w="40%"
                bg="#008080"
                isDisabled={loading}
                onPress={() => {
                  setLoading(true);
                  setTimeout(() => {
                    auth.logout(() => {
                      setLoading(false);
                      setModalVisible(false);
                      dropdownAlert.current.alertWithType('success', 'Log out successfully.');
                    });
                  }, 1000);
                }}
                ml="2"
              >
                Logout
              </Button>
            </HStack>
          </Modal.Body>
        </Modal.Content>
      </Modal>
    </>
  );
}

export default LogOut;