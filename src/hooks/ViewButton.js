import React, { useState } from 'react';
import { Pressable, Modal, Button, Text, HStack, VStack } from 'native-base';

function ViewButton({ children, details }) {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <Pressable onPress={() =>setShowModal(true)} bg="white">
        {children}
      </Pressable>
      <Modal size="xl" isOpen={showModal} onClose={() => setShowModal(false)}>
        <Modal.Content maxWidth="400px">
          <Modal.CloseButton />
          <Modal.Header>Details</Modal.Header>
          <Modal.Body>
            <VStack space={3} p="3">
              {Object.keys(details || {}).map(key => (
                <HStack direction="row" justifyContent="space-between" w="100%">
                  <VStack style={{ flexShrink: 1, width: '50%' }}>
                    <Text>{key}:</Text>
                  </VStack>
                  <VStack style={{ flexShrink: 1, width: '50%' }}>
                    {Array.isArray(details[key]) ? (
                      <>
                        {details[key].map(detail => (detail ? <Text>{detail}</Text> : null))}
                      </>
                    ) : <Text>{details[key]}</Text>}
                  </VStack>                
                </HStack>
              ))}
            </VStack>
          </Modal.Body>
          <Modal.Footer>
            <Button.Group space={2}>
              <Button
                variant="ghost"
                colorScheme="blueGray"
                onPress={() => {
                  setShowModal(false)
                }}
              >
                Close
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </>
  )
}

export default ViewButton;