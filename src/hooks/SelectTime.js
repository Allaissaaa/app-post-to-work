import React, { useState, useEffect } from 'react';
import { Select, HStack } from 'native-base';
import moment from 'moment';

function SelectTime({ value = null, onValueChange }) {
  const [hour, setHour] = useState(value ? parseInt(moment(value).format('hh')) : '');
  const [minute, setMinute] = useState(value ? parseInt(moment(value).format('mm')) : '');
  const [am_pm, setAMPM] = useState(value ? parseInt(moment(value).format('a')) : '');

  console.log(value, am_pm);

  useEffect(() => {
    if (hour && minute && am_pm) {
      onValueChange(convertTime12To24(hour + ':' + minute + ' ' + am_pm));
    }
  }, [hour, minute, am_pm]);

  const convertTime12To24 = time => {
    let hours   = Number(time.match(/^(\d+)/)[1]);
    let minutes = Number(time.match(/:(\d+)/)[1]);
    let AMPM    = time.match(/\s(.*)$/)[1];
    if (AMPM === "PM" && hours < 12) hours = hours + 12;
    if (AMPM === "AM" && hours === 12) hours = hours - 12;
    let sHours = hours.toString();
    let sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    return (sHours + ":" + sMinutes);
  };

  return (
    <HStack space={3} w="100%">
      <Select 
        flex={1}
        size="lg"
        bg="#eeeeee"
        color="#000000"
        variant="rounded"
        selectedValue={hour}
        placeholder="Hour" 
        onValueChange={text => setHour(text)}
      >
        {Array.from({ length: 12 }, (_, i) => i + 1).map(_hour => <Select.Item label={_hour.toString()} value={_hour} />)}
      </Select>
      <Select 
        flex={1}
        size="lg"
        bg="#eeeeee"
        color="#000000"
        variant="rounded"
        selectedValue={minute}
        placeholder="Minute" 
        onValueChange={text => setMinute(text)}
      >
        {Array.from({ length: 59 }, (_, i) => i + 1).map(_minute => <Select.Item label={_minute.toString()} value={_minute} />)}
      </Select>
      <Select 
        flex={1}
        size="lg"
        bg="#eeeeee"
        color="#000000"
        variant="rounded"
        selectedValue={am_pm}
        placeholder="AM/PM" 
        onValueChange={text => setAMPM(text)}
      >
        {['AM', 'PM'].map(_am_pm => <Select.Item label={_am_pm.toString()} value={_am_pm} />)}
      </Select>
    </HStack>
  )
}

export default SelectTime;