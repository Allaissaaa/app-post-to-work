import React, { useState, useEffect } from 'react';
import { Select, HStack } from 'native-base';
import moment from 'moment';

function SelectDate({ value = null, future = false, onValueChange }) {
  const [day, setDay] = useState(value ? parseInt(moment(value).format('D')) : '');
  const [month, setMonth] = useState(value ? parseInt(moment(value).format('M')) : '');
  const [year, setYear] = useState(value ?parseInt(moment(value).format('YYYY')) : '');

  useEffect(() => {
    if (day && month && year) {
      onValueChange(year + '-' + month + '-' + day);
    }
  }, [day, month, year]);

  const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  const years = future ? Array.from({ length: 51 }, (_, i) => new Date().getFullYear() + i) : Array.from({ length: 51 }, (_, i) => new Date().getFullYear() - i);

  return (
    <HStack space={3} w="100%">
      <Select 
        flex={1}
        size="lg"
        bg="#eeeeee"
        color="#000000"
        variant="rounded"
        selectedValue={day}
        placeholder="Day" 
        onValueChange={text => setDay(text)}
      >
        {Array.from({ length: 31 }, (_, i) => i + 1).map(_day => <Select.Item label={_day.toString()} value={_day} />)}
      </Select>
      <Select 
        flex={1}
        size="lg"
        bg="#eeeeee"
        color="#000000"
        variant="rounded"
        selectedValue={month}
        placeholder="Month" 
        onValueChange={text => setMonth(text)}
      >
        {Array.from({ length: 12 }, (_, i) => i + 1).map(_month => <Select.Item label={months[_month - 1].toString()} value={_month} />)}
      </Select>
      <Select 
        flex={1}
        size="lg"
        bg="#eeeeee"
        color="#000000"
        variant="rounded"
        selectedValue={year}
        placeholder="Year" 
        onValueChange={text => setYear(text)}
      >
        {years.map(_year => <Select.Item label={_year.toString()} value={_year} />)}
      </Select>
    </HStack>
  )
}

export default SelectDate;