import React, { useState } from 'react';
import { Pressable, VStack, Text, AlertDialog, Button, Center } from 'native-base';

function CancelButton({ onPress, ...props }) {
  
  const [isOpen, setIsOpen] = useState(false);

  const onOpen = () => setIsOpen(true);

  const onClose = () => setIsOpen(false);

  const onClick = () => {
    onPress();
    onClose();
  };

  return (
    <>
      <Pressable
        w="70"
        cursor="pointer"
        bg="red.500"
        justifyContent="center"
        onPress={onOpen}
        _pressed={{
          opacity: 0.5,
        }}
        {...props}
      >
        <VStack alignItems="center" space={2}>
          <Text color="white" fontSize="xs" fontWeight="medium">
            Cancel
          </Text>
        </VStack>
      </Pressable>
      <Center>
        <AlertDialog
          isOpen={isOpen}
          onClose={onClose}
        >
          <AlertDialog.Content>
            <AlertDialog.CloseButton />
            <AlertDialog.Header>Cancel</AlertDialog.Header>
            <AlertDialog.Body>
              Are you sure you want to delete this?
            </AlertDialog.Body>
            <AlertDialog.Footer>
              <Button.Group space={2}>
                <Button
                  variant="unstyled"
                  colorScheme="coolGray"
                  onPress={onClose}
                >
                  Cancel
                </Button>
                <Button colorScheme="danger" onPress={onClick}>
                  Cancel
                </Button>
              </Button.Group>
            </AlertDialog.Footer>
          </AlertDialog.Content>
        </AlertDialog>
      </Center>
    </>
  )
}

export default CancelButton;