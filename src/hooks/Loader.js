import React from 'react';
import {
  Spinner,
  HStack,
  Heading,
  Center,
  NativeBaseProvider,
} from 'native-base';

export default () => {
  return (
    <Center flex={1} px="3">
      <HStack space={2} alignItems="center" p="4">
        <Spinner />
        <Heading color="#74BEB5" fontSize="md">
          Loading
        </Heading>
      </HStack>
    </Center>
  )
}
