import React, { useState, useEffect } from 'react';
import DropDown from 'react-native-dropdown-picker';

function DropDownPicker({ 
  options = [], 
  onValueChange = null, 
  placeholder = "Select an item",
  searchable = false,
  selectedValue = null,
  listMode = "FLATLIST",
  flatListProps = {}
}) {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(selectedValue);
  const [items, setItems] = useState(options);

  useEffect(() => {
    if (onValueChange) {
      onValueChange(value);
    }
  }, [value]);

  useEffect(() => {
    setValue(selectedValue);
  }, [selectedValue]);

  return (
    <DropDown
      placeholder={placeholder}
      open={open}
      value={value}
      items={options}
      setOpen={setOpen}
      setValue={setValue}
      setItems={setItems}
      searchable={searchable}
      listMode={listMode}
      flatListProps={flatListProps}
    />
  );
}

export default DropDownPicker;