import React, { useState } from 'react';
import { Box, Pressable, VStack, Text, Actionsheet, useDisclose, Button, Center } from 'native-base';

function MoreButton({ onPress, items }) {
  const { isOpen, onOpen, onClose } = useDisclose();

  const onClick = item => () => {
    onClose();
    onPress(item);
  }

  return (
    <>
      <Actionsheet isOpen={isOpen} onClose={onClose}>
        <Actionsheet.Content>
          <Box w="100%" h={60} px={4} justifyContent="center">
            <Text
              fontSize="16"
              color="gray.500"
              _dark={{
                color: "gray.300",
              }}
            >
              More
            </Text>
          </Box>
          {items.map(item => <Actionsheet.Item onPress={onClick(item)}>{item}</Actionsheet.Item>)}
        </Actionsheet.Content>
      </Actionsheet>
      <Pressable
        w="70"
        ml="auto"
        cursor="pointer"
        bg="coolGray.200"
        justifyContent="center"
        onPress={onOpen}
        _pressed={{
          opacity: 0.5,
        }}>
        <VStack alignItems="center" space={2}>
          <Text fontSize="xs" fontWeight="medium" color="coolGray.800">
            More
          </Text>
        </VStack>
      </Pressable>
    </>
  )
}

export default MoreButton;