import React, { useState } from 'react';
import { Pressable, VStack, Text, AlertDialog, Button, Center } from 'native-base';

function RestoreButton({ onPress, ...props }) {
  
  const [isOpen, setIsOpen] = useState(false);

  const onOpen = () => setIsOpen(true);

  const onClose = () => setIsOpen(false);

  const onClick = () => {
    onPress();
    onClose();
  };

  return (
    <>
      <Pressable
        w="70"
        cursor="pointer"
        bg="green.500"
        justifyContent="center"
        onPress={onOpen}
        _pressed={{
          opacity: 0.5,
        }}
        {...props}
      >
        <VStack alignItems="center" space={2}>
          <Text color="white" fontSize="xs" fontWeight="medium">
            Restore
          </Text>
        </VStack>
      </Pressable>
      <Center>
        <AlertDialog
          isOpen={isOpen}
          onClose={onClose}
        >
          <AlertDialog.Content>
            <AlertDialog.CloseButton />
            <AlertDialog.Header>Restore</AlertDialog.Header>
            <AlertDialog.Body>
              Are you sure you want to restore this?
            </AlertDialog.Body>
            <AlertDialog.Footer>
              <Button.Group space={2}>
                <Button
                  variant="unstyled"
                  colorScheme="coolGray"
                  onPress={onClose}
                >
                  Cancel
                </Button>
                <Button bg="green.500" onPress={onClick}>
                  Restore
                </Button>
              </Button.Group>
            </AlertDialog.Footer>
          </AlertDialog.Content>
        </AlertDialog>
      </Center>
    </>
  )
}

export default RestoreButton;