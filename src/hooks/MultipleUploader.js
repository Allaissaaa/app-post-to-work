import React, { useState } from 'react';
import { Pressable } from 'native-base';
import DocumentPicker from 'react-native-document-picker';

function MultipleUploader({ children }) {

  const openFileBrowser = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.images],
      })
      for (const res of results) {
        console.log(
          res.uri,
          res.type, // mime type
          res.name,
          res.size,
        )
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err
      }
    }
  };

  return (
    <Pressable onPress={openFileBrowser} bg="white">
      {children}
    </Pressable>
  )
}

export default MultipleUploader;