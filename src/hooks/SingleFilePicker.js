import React, { useState } from 'react';
import { Pressable } from 'native-base';
import DocumentPicker from 'react-native-document-picker';

function SingleFilePicker({ children, onFileSelect }) {

  const openFileBrowser = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      })
      const file = res[0];
      onFileSelect(file);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err
      }
    }
  };

  return (
    <Pressable onPress={openFileBrowser}>
      {children}
    </Pressable>
  )
}

export default SingleFilePicker;