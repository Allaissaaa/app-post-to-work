import React, { useEffect, useState, useContext } from 'react';
// import { NavigationContext } from 'react-navigation';
import Dialog from 'react-native-dialog';
import codePush from 'react-native-code-push';

function CodePushUpdater() {
  // const navigation = useContext(NavigationContext);
  const [visible, setVisible] = useState(false);

  const checkCodePushVersion = async () => {
    const update = await codePush.checkForUpdate();
    if (update) {
      setTimeout(() => {
        // setVisible(true);
      }, 1000);
    }
  };

  useEffect(() => {
    checkCodePushVersion();
  }, []);

  const handleUpdate = () => {
    // navigation.navigate('CheckForUpdates');
    setVisible(false);
  };

  return (
    <Dialog.Container visible={visible}>
      <Dialog.Title>Update Available</Dialog.Title>
      <Dialog.Description>
        We have an app update! Click Update button in order to use the app properly and without hazzle.
      </Dialog.Description>
      <Dialog.Button label="Update" onPress={handleUpdate} />
    </Dialog.Container>
  );
}

export default codePush({ checkFrequency: codePush.CheckFrequency.MANUAL })(CodePushUpdater);
