import React, { useState, useEffect } from 'react';
import DatePicker from 'react-native-date-picker';
import { Pressable, Input, HStack } from 'native-base';
import moment from 'moment';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default ({ onChange, value, mode, minuteInterval, modal, format, style, inputProps, iconProps, variant = "outline" }) => {
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);

  const onConfirm = date => {
    setOpen(false);
    setDate(date);
    onChange(date);
  };

  const onCancel = () => setOpen(false);

  useEffect(() => {
    setDate(value);
  }, [value]);

  const icon = {
    time: 'clock',
    date: 'calendar',
    datetime: 'calendar',
  };

  return (
    <>
      {modal ? (
      <Pressable onPress={() => setOpen(true)}>
        <Input
          variant={variant}
          value={value ? moment(date).format(format || 'MMMM DD, YYYY').toString() : ''}
          placeholder="Select date..."
          InputRightElement={
            <Icon 
              name={icon[mode]} 
              color="#eb8258"
              size={25} 
              style={{
                marginRight: 5
              }}
              {...iconProps}
            />
          }
          isReadOnly
          {...inputProps}
        />
      </Pressable>
      ) : null}
      <DatePicker
        mode={mode}
        modal={modal}
        open={open}
        date={value ? new Date(date) : new Date()}
        onConfirm={onConfirm}
        onCancel={onCancel}
        {...(minuteInterval ? { minuteInterval } : {})}
      />
    </>
  )
}