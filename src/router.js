import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { View, ScrollView, TouchableOpacity, StatusBar, Linking } from 'react-native';
import { Text, Avatar, Image, Box, Center, HStack, Pressable, Heading, Menu, Badge } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import OneSignal from 'react-native-onesignal';
import { dispatch } from 'use-bus';
import { SafeAreaView } from 'react-navigation';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { useAuth } from './helpers/auth';
import { useDispatch, useSelector } from 'react-redux';
import { updateState } from './helpers/updateState';
import { useGetProfileQuery } from './redux/services/profile';
import { storeUser, fetchUser  } from './redux/reducers/user';
import { useNavigation } from '@react-navigation/native';
import { PortalProvider } from '@gorhom/portal';
import LandingPage from './routes/LandingPage';
import LogIn from './routes/Login';
import Register from './routes/Register';
import VerifyContactNoSendOTP from './routes/VerifyContactNoSendOTP';
import Dashboard from './routes/Dashboard';
import Notifications from './routes/Notifications';
import Messages from './routes/Messages';
import Profile from './routes/Account/Profile';
import Password from './routes/Account/Password';
import Echo from './helpers/echo';
import Loader from './routes/Shared/Loader';
import LogOut from './hooks/LogOut';
import { fetchNotifications } from './redux/reducers/notifications';
import { check, request, PERMISSIONS } from 'react-native-permissions';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const options = (title = null, auth, back = false, headerShown = true) => ({ 
  route, 
  navigation 
}) => (({
  headerShown,
  headerTitle: (props) => null,
  headerStyle: {
    backgroundColor: '#002460',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomWidth: 0
  },
  headerLeft: props => (
    <HStack direction="row" justifyContent="flex-start" alignItems="center">
      {back ? (
        <TouchableOpacity
          onPress={() => navigation.goBack()} 
        >
          <Icon 
            name="arrow-left"
            color="#ffffff"
            size={25}
            style={{
              marginLeft: 10,
              marginRight: 20
            }}
          />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          onPress={() => navigation.toggleDrawer()} 
        >
          <Icon 
            name="menu"
            color="#ffffff"
            size={25}
            style={{
              marginLeft: 10,
              marginRight: 20
            }}
          />
        </TouchableOpacity>
      )}
      <>
        {(() => {
          const header_title = route.params?.title || props.title || title || '';

          return (
            <Text
              style={{
                width: 200,
                fontWeight: 'bold',
                fontSize: 20,
                color: '#ffffff',
              }}
              numberOfLines={1} 
              ellipsizeMode="tail"
            >
              {header_title}
            </Text>
          )
        })()}
      </>
    </HStack>
  ),
  headerRight: () => null,
}))

function CustomDrawerContent(props) {
  const auth = useAuth();
  const dispatch = useDispatch();

  const navigate = (options) => {
    props.navigation.navigate(options);
    setTimeout(() => {
      props.navigation.toggleDrawer();
    }, 500);
  };

  return (
    <DrawerContentScrollView 
      {...props} 
      contentContainerStyle={{ 
        flex: 1, 
        backgroundColor: '#002460', 
        flexDirection: 'column', 
        justifyContent: 'space-between' 
      }}
    >
      <ScrollView  forceInset={{ top: 'always', horizontal: 'never' }}>
        <Box
          pl="5"
          pr="5"
        >
          <HStack
            justifyContent="flex-end"
          >
            <Pressable
              onPress={() => props.navigation.toggleDrawer()}
            >
              <Text
                _light={{ color: '#ffffff' }}
                _dark={{ color: '#ffffff' }}
              >
                X Close
              </Text>
            </Pressable>
          </HStack>
          <Pressable
            onPress={() => navigate({
              name: 'Profile',
              params: {
                id: auth.getId,
              },
              key: (Math.random() * 10000).toString(),
            })}
          >
          <HStack
            alignItems="center"
          >
            <Image
              key={auth.getAvatar}
              source={{
                uri: auth.getAvatar,
              }}
              borderRadius={60}
              alt="SS"
              size={60}
              style={{
                marginRight: 10,
                backgroundColor: '#aeaeae',
              }}
              mb="3"
            />
            <Box
              ml="2"
            >
              <Heading 
                size="md" 
                mb="1"
                _light={{ color: '#ffffff' }}
                _dark={{ color: '#ffffff' }}
              >
                {auth.getName}
              </Heading>
              <Text
                fontSize="xs"
                _light={{ color: '#ffffff' }}
                _dark={{ color: '#ffffff' }}
                fontWeight="500"
              >
                {auth.getEmail}
              </Text>
             <Text
                fontSize="xs"
                _light={{ color: '#ffffff' }}
                _dark={{ color: '#ffffff' }}
                fontWeight="500"
              >
                {auth.getRole}
              </Text>
            </Box>
          </HStack>
          </Pressable>
        </Box>
        <Box mt="5" mb="5" ml="2" mr="2">
          {auth.isClient && (
            <>
              <DrawerItem 
                label={() => (
                  <HStack direction="row" justifyContent="flex-start" w="100%">
                    <Icon 
                      name="account" 
                      color="#ffffff" 
                      size={25} 
                      style={{
                        marginRight: 10
                      }}
                    />
                    <Text
                      bold
                      _light={{ color: '#ffffff' }}
                      _dark={{ color: '#ffffff' }}
                    >
                      Job Lists
                    </Text>
                  </HStack>
                )}
                onPress={() => {}} 
              />
            </>
          )}
          {auth.isFreelancer && (
            <>
              <DrawerItem 
                label={() => (
                  <HStack direction="row" justifyContent="flex-start" w="100%">
                    <Icon 
                      name="file" 
                      color="#ffffff" 
                      size={25} 
                      style={{
                        marginRight: 10
                      }}
                    />
                    <Text
                      bold
                      _light={{ color: '#ffffff' }}
                      _dark={{ color: '#ffffff' }}
                    >
                      Job Applications
                    </Text>
                  </HStack>
                )}
                onPress={() => {}} 
              />
            </>
          )}
          <DrawerItem 
            label={() => (
              <HStack direction="row" justifyContent="flex-start" w="100%">
                <Icon 
                  name="account" 
                  color="#ffffff" 
                  size={25} 
                  style={{
                    marginRight: 10
                  }}
                />
                <Text
                  bold
                  _light={{ color: '#ffffff' }}
                  _dark={{ color: '#ffffff' }}
                >
                  Update Profile
                </Text>
              </HStack>
            )}
            onPress={() => navigate('Profile')} 
          />
          <DrawerItem 
            label={() => (
              <HStack direction="row" justifyContent="flex-start" w="100%">
                <Icon 
                  name="lock" 
                  color="#ffffff" 
                  size={25} 
                  style={{
                    marginRight: 10
                  }}
                />
                <Text
                  bold
                  _light={{ color: '#ffffff' }}
                  _dark={{ color: '#ffffff' }}
                >
                  Update Password
                </Text>
              </HStack>
            )}
            onPress={() => navigate('Password')} 
          />
          <LogOut>
            {setIsOpen => (
              <DrawerItem
                label={() => (
                  <HStack direction="row" justifyContent="flex-start" w="100%">
                    <Icon 
                      name="logout" 
                      color="#ffffff" 
                      size={25}
                      style={{
                        marginRight: 10
                      }}
                    />
                    <Text
                      bold
                      _light={{ color: '#ffffff' }}
                      _dark={{ color: '#ffffff' }}
                    >
                      Logout
                    </Text>
                  </HStack>
                )}
                onPress={() => {
                  setIsOpen(true);
                  setTimeout(() => {
                    props.navigation.toggleDrawer();
                  }, 0);
                }} 
              />
            )}
          </LogOut>
        </Box>
      </ScrollView>
    </DrawerContentScrollView>
  );
}

function Main() {
  const dispatch = useDispatch();
  const auth = useAuth();
  const notifications = useSelector(state => state.notifications.value);

  console.log(notifications);

  useEffect(() => {
    Echo.join('global')
    .here(users => {
      console.log('users here', users);
    })
    .joining(user => {
      console.log('joining here', user);
    })
    .leaving(user => {
      console.log('leaving here', user);
    });

    return () => {
      Echo.leave('global');
      Echo.leaveChannel('global');
    }
  }, []);

  useEffect(() => {
    OneSignal.setNotificationWillShowInForegroundHandler(notifReceivedEvent => {
      console.log('OneSignal: notification will show in foreground:', notifReceivedEvent);
      dispatch(fetchUser());
      dispatch(fetchNotifications());
      let notif = notifReceivedEvent.getNotification();
      setTimeout(() => notifReceivedEvent.complete(notif), 0);
    });
    OneSignal.setNotificationOpenedHandler(openedEvent => {
      console.log('OneSignal: notification opened:', openedEvent);
      const { notification } = openedEvent;
      console.log(notification);
      // this.navigate(notification.payload.launchURL);
    });
    OneSignal.disablePush(false);
    OneSignal.promptLocation();
    if (auth.isAuthenticated) {
      dispatch(fetchUser());
      OneSignal.setExternalUserId(auth.getId?.toString());
      OneSignal.sendTags({
        name: auth.getName,
        user_id: auth.getId,
        role_id: auth.getType,
      });
    } else {
      OneSignal.deleteTag('name');
      OneSignal.deleteTag('user_id');
      OneSignal.deleteTag('role_id');
      OneSignal.clearOneSignalNotifications();
      OneSignal.removeExternalUserId();
    }
  }, [auth.isAuthenticated]);

  return (
    <>
      <StatusBar backgroundColor="#002460" />
      <Tab.Navigator
        initialRouteName="Dashboard"
        tabBarOptions={{
          showLabel: false,
          activeTintColor: '#002460',
          inactiveTintColor: '#002460',
        }}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Dashboard') {
              iconName = focused ? 'home' : 'home-outline';
            }  else if (route.name === 'Notifications') {
              iconName = focused ? (notifications ? 'bell-badge' : 'bell') : (notifications ? 'bell-badge' : 'bell');
            }  else if (route.name === 'Messages') {
              iconName = focused ? 'message' : 'message';
            }

            return <Icon name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: '#002460',
          tabBarInactiveTintColor: 'gray',
        })}
      >
        <Tab.Screen 
          name="Dashboard" 
          options={options('Post to Work', auth)} 
          component={Dashboard} 
        />
        <Tab.Screen 
          name="Notifications" 
          options={options('Notifications', auth)} 
          component={Notifications} 
        />
        <Tab.Screen 
          name="Messages" 
          options={options('Messages', auth)} 
          component={Messages}
        />
      </Tab.Navigator>
    </>
  );
}

function Home() {
  return (
    <Drawer.Navigator
      initialRouteName="Main"
      drawerContent={(props) => <CustomDrawerContent {...props} />}
      drawerType="front"
    >
      <Drawer.Screen options={{ headerShown: false }} name="Main" component={Main} />
    </Drawer.Navigator>
  )
}

export default function Router() {
  const auth = useAuth();

  const requestNotificationPermission = async () => {
    const result = await request(PERMISSIONS.ANDROID.POST_NOTIFICATIONS);
    return result;
  };

  const checkNotificationPermission = async () => {
    const result = await check(PERMISSIONS.ANDROID.POST_NOTIFICATIONS);
    return result;
  };

  useEffect(() => {
    requestNotificationPermission();
  }, []);

  return (
    <NavigationContainer>
      <StatusBar backgroundColor="#002460" />
      <Stack.Navigator>
        {auth?.isAuthenticated ? (
          <>
            <Stack.Screen options={{ headerShown: false }} name="Home" component={Home} />
            <Stack.Screen options={options('Update Profile', auth, true)} name="Profile" component={Profile} />
            <Stack.Screen options={options('Update Password', auth, true)} name="Password" component={Password} />
          </>
        ) : (
          <>
            <Stack.Screen options={{ headerShown: false }} name="LandingPage" component={LandingPage} />
            <Stack.Screen options={options('Login', auth, true)} name="LogIn" component={LogIn} />
            <Stack.Screen options={options('Register', auth, true)} name="Register" component={Register} />
            <Stack.Screen options={options('Verify Contact No', auth, true)} name="VerifyContactNoSendOTP" component={VerifyContactNoSendOTP} />
          </>
        )}        
      </Stack.Navigator>
    </NavigationContainer>
  )
}