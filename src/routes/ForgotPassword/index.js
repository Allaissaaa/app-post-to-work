import React, { useState, useCallback, useMemo, useRef } from 'react';
import { View, ActivityIndicator, Dimensions } from 'react-native';
import { Pressable, Heading, WarningOutlineIcon, Box, VStack, Image, Text, Button, FormControl, Input, Link, Center } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch } from 'react-redux';
import { storeUser } from '../../redux/reducers/user';
import { storeToken } from '../../redux/reducers/token';
import { updateState } from '../../helpers/updateState';
import { useDropDown } from '../../context/DropDownComponentProvider';
import { hasError } from '../../helpers/utils';
import { useForgotPasswordMutation } from '../../redux/services/login';

const { width } = Dimensions.get('window');

function ForgotPassword({ navigation }) {
  const dispatch = useDispatch();
  const { dropdownAlert } = useDropDown();
  const [forgotPassword] = useForgotPasswordMutation();
  const [state, setState] = useState({
    email: '',
  });
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});

  const onChangeText = (key, value) => {
    setState(prevState => ({
      ...prevState,
      [key]: value,
    }));
  }  

  const submit = async () => {
    setLoading(true);
    setErrors({});
    try {
      const response = await forgotPassword(state).unwrap();
      if (response.success) {
        dropdownAlert.current.alertWithType('success', 'Please check your email for password reset link.');
        navigation.goBack();
      } else {
        dropdownAlert.current.alertWithType('error', '');
      }
      setLoading(false);
    } catch (error) {
      console.log(error);
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      setLoading(false);
    }
  }

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={{
        flexGrow: 1,
        backgroundColor: '#ffffff',
      }}
      keyboardShouldPersistTaps="always"
    >
      <Box
        style={{
          flex: 1,
        }}
      >
        <Center
          p="10"
        >
          <Image
            source={require('../../assets/logo.png')}
            style={{ 
              width: 200, 
              height: 200,
            }}
            PlaceholderContent={<ActivityIndicator />}
            alt="logo"
          />
          <Heading size="2xl">Forgot</Heading>
          <Heading size="2xl" mb="3">Password?</Heading>
          <Heading size="sm">New Password</Heading>
        </Center>
        <Box
          bg="#ffffff"
          style={{
            width: '100%',
          }}
        >
          <VStack 
            borderRadius="50"
            space={3}
            w="100%" 
            p={5}
            mb="5"
          >
            <FormControl isInvalid={hasError('email', errors)} mb="10">
              <FormControl.Label _text={{ color: '#aeaeae'}} mb="3">ENTER YOUR EMAIL</FormControl.Label>
              <Input
                size="lg"
                bg="#eeeeee"
                color="#000000"
                variant="rounded"
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                value={state.email}
                placeholder="Email"
                onChangeText={text => onChangeText('email', text)}
              />
              {hasError('email', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('email', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <Center>
              <Button
                p="5"
                borderRadius="50"
                w={width - (width / 3)}
                bg="#008080"
                _text={{ color: '#ffffff'}}
                onPress={submit}
                isLoading={loading}
              >
                send
              </Button>
            </Center>
          </VStack>
          <Center>
            <Button
              variant="ghost"
              borderRadius="50"
              _text={{ color: '#aeaeae'}}
              w={200}
              onPress={() => navigation.goBack()}
            >
              BACK
            </Button>
          </Center>
        </Box>
      </Box>
    </KeyboardAwareScrollView>
  );
}

export default ForgotPassword;