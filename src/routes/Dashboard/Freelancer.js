import React, { useState, useContext, useEffect, useRef } from 'react';
import { View, StatusBar, Platform, FlatList, SectionList } from 'react-native';
import axios from '../../helpers/axios';
import { useIsFocused } from '@react-navigation/native';
import { Box, Text, Pressable, Heading, IconButton, HStack, Avatar, VStack, Center, Button, Spacer, Divider } from 'native-base';
import { SwipeListView } from 'react-native-swipe-list-view';
import { useDispatch } from 'react-redux';
import { useGetJobPostsQuery } from '../../redux/services/job-posts';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { timeAgo } from '../../helpers/utils';
import { useAuth } from '../../helpers/auth';
import { icons } from '../Shared/DefaultIcons';
import moment from 'moment';

function RenderItem({ item, index }) {
  return (
    <Pressable onPress={() => {}} pl="5" pr="5" pt="3" pb="5" mb="3" borderBottomWidth="1" borderBottomColor="#eeeeee">
      <Box border="1" borderRadius="md">
        <VStack space="4">
          <Box px="4" pt="4">
            <Heading>{item.title}</Heading>
          </Box>
          <Box px="4">{item.description}</Box>
          <Box px="4" pb="4">
            {`Company: ${item.company_information.name}`}
            {`Posted on ${item.created_at_formatted}`}
          </Box>
          <Button
            p="3"
            size="lg"
            onPress={() => {}}
          >
            Apply Now
          </Button>
        </VStack>
      </Box>
    </Pressable>
  )
}

function Freelancer({ navigation }) {
  const auth = useAuth();
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const { data, error, isLoading, isFetching, refetch } = useGetJobPostsQuery({
    page,
    per_page: 10,
  });

  useEffect(() => {
    refetch();
  }, []);

  return (
    <Box
      style={{
        flex: 1,
        backgroundColor: '#ffffff',
      }}
    >
      {error ? (
        <Text>Oh no, there was an error</Text>
      ) : isLoading ? (
        <Text></Text>
      ) : data ? (
        <FlatList
          data={data.data}
          renderItem={({ item, index }) => <RenderItem item={item} index={index} />}
          keyExtractor={item => item.id}
          onRefresh={refetch}
          refreshing={isLoading}
          ListHeaderComponent={null}
          ListFooterComponent={
            <HStack justifyContent="center" space={3} p={3}>
              <Button
                p="3"
                borderRadius="50"
                size="lg"
                variant="ghost"
                onPress={() => setPage(page - 1)}
                isDisabled={data?.prev_page_url ? false : true}
                isLoading={isFetching}
              >
                Previous
              </Button>
              <Button
                p="3"
                borderRadius="50"
                size="lg"
                variant="ghost"
                onPress={() => setPage(page + 1)}
                isDisabled={data?.next_page_url ? false : true}
                isLoading={isFetching}
              >
                Next
              </Button>
            </HStack>
          }
        />
      ) : null}
    </Box>
  );
}

export default Freelancer;
