import React, { useState, useCallback, useMemo, useRef, useEffect } from 'react';
import { View, ActivityIndicator, ScrollView, useWindowDimensions, FlatList } from 'react-native';
import { Alert, Pressable, Heading, WarningOutlineIcon, Box, VStack, Image, Text, Button, FormControl, Input, Link, Center, HStack, Avatar } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useAuth } from '../../helpers/auth';
import { useHeaderHeight } from '@react-navigation/elements';
import { useDropDown } from '../../context/DropDownComponentProvider';
import LinearGradient from 'react-native-linear-gradient';
import Client from './Client';
import Freelancer from './Freelancer';

function Dashboard({ navigation }) {
  const { width } = useWindowDimensions();
  const auth = useAuth();
  const { dropdownAlert } = useDropDown();

  const greetings = (type = null) => {
    let greetdate = new Date();
    let hours = greetdate.getHours();

    if (hours >= 5 && hours <= 11) {
      return type === 'icon' ? ['#fce354', 'weather-sunny'] : 'Good Morning';
    } else if (hours === 12) {
      return type === 'icon' ? ['#fce354', 'weather-sunny'] : 'Good Afternoon';
    } else if (hours >= 13 && hours <= 17) {
      return type === 'icon' ? ['#fce354', 'weather-sunny'] : 'Good Afternoon';
    } else if (hours >= 18 && hours <= 20) {
      return type === 'icon' ? ['#fce354', 'weather-night'] : 'Good Evening';
    } else if (hours >= 21 && hours <= 11) {
      return type === 'icon' ? ['#fce354', 'weather-night'] : 'Good Evening';
    } else {
      return type === 'icon' ? ['#fce354', 'weather-night'] : 'Good Evening';
    }
  }

  if (auth.isClient) {
    return <Client />
  }

  return <Freelancer />;
}

export default Dashboard;