import React, { useState } from 'react';
import { View, ActivityIndicator, useWindowDimensions } from 'react-native';
import { Pressable, Heading, WarningOutlineIcon, Box, VStack, Image, Text, Button, FormControl, Input, Link, Center, HStack } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch, useSelector } from 'react-redux';
import { storeUser } from '../../redux/reducers/user';
import { storeToken } from '../../redux/reducers/token';
import { updateState } from '../../helpers/updateState';
import { useDropDown } from '../../context/DropDownComponentProvider';
import { hasError } from '../../helpers/utils';
import { useLoginMutation } from '../../redux/services/login';

function Login({ navigation }) {
  const { width } = useWindowDimensions();
  const dispatch = useDispatch();
  const { dropdownAlert } = useDropDown();
  const [login] = useLoginMutation();
  const [state, setState] = useState({
    email: '',
    password: '',
  });
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});

  const onChangeText = (key, value) => {
    setState(prevState => ({
      ...prevState,
      [key]: value,
    }));
  }  

  const submit = async () => {
    setLoading(true);
    setErrors({});
    try {
      const response = await login(state).unwrap();
      if (response.success) {
        if (response?.not_verified) {
          navigation.navigate('Verify Email', {
            email: response.email,
            token: response.token,
          });
        } else {
          await dispatch(updateState(storeUser(response.user)));
          await dispatch(updateState(storeToken(response.token)));
        }
      } else {
        dropdownAlert.current.alertWithType('error', response.message || 'Error');
      }
      setLoading(false);
    } catch (error) {
      console.log(error);
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      setLoading(false);
    }
  }

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={{
        flexGrow: 1,
      }}
      keyboardShouldPersistTaps="always"
    >
      <Center
        style={{
          flex: 1,
        }}
      >
        <Center
          mt="10"
        >
          <Image
            source={require('../../assets/logo.png')}
            style={{ 
              width: 200, 
              height: 200,
            }}
            PlaceholderContent={<ActivityIndicator />}
            alt="logo"
            mb="6"
          />
          <Heading textAlign="center">Sign in to continue</Heading>
        </Center>
        <Box
          style={{
            width: '100%',
          }}
        >
          <VStack 
            space="5"
            w="100%" 
            p="5"
          >
            <FormControl isInvalid={hasError('email', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                keyboardType="email-address"
                value={state.email}
                placeholder="Email"
                onChangeText={text => onChangeText('email', text)}
              />
              {hasError('email', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('email', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('password', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                type="password"
                value={state.password}
                placeholder="Password"
                onChangeText={text => onChangeText('password', text)}
              />
              {hasError('password', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('password', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <Button
              size="lg"
              w="100%"
              bg="#dc143b"
              _text={{ color: '#ffffff'}}
              onPress={submit}
              isLoading={loading}
            >
              Login
            </Button>
          </VStack>
        </Box>
      </Center>
    </KeyboardAwareScrollView>
  );
}

export default Login;