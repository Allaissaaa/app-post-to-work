import React, { useState, useContext, useEffect, useRef } from 'react';
import { View, StatusBar, Platform, FlatList, SectionList } from 'react-native';
import axios from '../../helpers/axios';
import { useIsFocused } from '@react-navigation/native';
import { Box, Text, Pressable, Heading, IconButton, HStack, Avatar, VStack, Center, Button, Spacer } from 'native-base';
import { SwipeListView } from 'react-native-swipe-list-view';
import { useDispatch } from 'react-redux';
import { fetchNotifications } from '../../redux/reducers/notifications';
import { useGetNotificationsQuery } from '../../redux/services/notifications';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { timeAgo } from '../../helpers/utils';
import { useAuth } from '../../helpers/auth';
import { icons } from '../Shared/DefaultIcons';
import moment from 'moment';

function RenderItem({ item, index }) {

  const open = item => () => {};

  return (
    <Pressable onPress={() => {}} pl="5" pr="5" pt="3" pb="5" mb="3" borderBottomWidth="1" borderBottomColor="#eeeeee">
      <Box>
        <HStack alignItems="flex-start" space="3">
          <Avatar size="48px" bg="#ffffff" source={icons[12]} alt={item?.content} />
          <VStack style={{ flexShrink: 1 }}>
            <Heading size="sm" numberOfLines={5} mb="1" bold>
              {item?.data?.content}
            </Heading>
            <Text color="coolGray.600" _dark={{ color: 'warmGray.200' }}>{moment(item.created_at).fromNow()}</Text>
          </VStack>
        </HStack>
      </Box>
    </Pressable>
  )
}

function Index({ navigation }) {
  const auth = useAuth();
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const { data, error, isLoading, isFetching, refetch } = useGetNotificationsQuery({
    page,
    per_page: 10,
  });

  useEffect(() => {
    refetch();
    dispatch(fetchNotifications());
  }, []);

  return (
    <Box
      style={{
        flex: 1,
        backgroundColor: '#ffffff',
      }}
    >
      {error ? (
        <Text>Oh no, there was an error</Text>
      ) : isLoading ? (
        <Text></Text>
      ) : data ? (
        <FlatList
          data={data.data}
          renderItem={({ item, index }) => <RenderItem item={item} index={index} />}
          keyExtractor={item => item.id}
          onRefresh={refetch}
          refreshing={isLoading}
          ListHeaderComponent={null}
          ListFooterComponent={
            <HStack justifyContent="center" space={3} p={3}>
              <Button
                p="3"
                borderRadius="50"
                size="lg"
                variant="ghost"
                onPress={() => setPage(page - 1)}
                isDisabled={data?.prev_page_url ? false : true}
                isLoading={isFetching}
              >
                Previous
              </Button>
              <Button
                p="3"
                borderRadius="50"
                size="lg"
                variant="ghost"
                onPress={() => setPage(page + 1)}
                isDisabled={data?.next_page_url ? false : true}
                isLoading={isFetching}
              >
                Next
              </Button>
            </HStack>
          }
        />
      ) : null}
    </Box>
  );
}

export default Index;