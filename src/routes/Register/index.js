import React, { useState, useCallback, useMemo, useRef, useEffect } from 'react';
import { View, StyleSheet, ActivityIndicator, useWindowDimensions } from 'react-native';
import { TextArea, Select, Radio, HStack, Pressable, Heading, WarningOutlineIcon, Box, VStack, Image, Text, Button, FormControl, Input, Link, Center, Checkbox } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch } from 'react-redux';
import { storeUser } from '../../redux/reducers/user';
import { storeToken } from '../../redux/reducers/token';
import { updateState } from '../../helpers/updateState';
import { useDropDown } from '../../context/DropDownComponentProvider';
import { hasError } from '../../helpers/utils';
import { useRegisterWithOtpMutation } from '../../redux/services/register';
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from '../Shared/DatePicker';

function Register({ route, navigation }) {
  const { width } = useWindowDimensions();
  const { role_id } = route.params ?? {};
  const dispatch = useDispatch();
  const { dropdownAlert } = useDropDown();
  const [registerWithOtp] = useRegisterWithOtpMutation();
  const [state, setState] = useState({
    first_name: '',
    last_name: '',
    address: '',
    gender: '',
    date_of_birth: '',
    contact_no: '',
    password: '',
    password_confirmation: '',
  });
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});

  useEffect(() => {
    navigation.setParams({ 
      title: { 2: 'Register as Client', 3: 'Register as Freelancer' }[role_id],
    });
  }, []);

  const onChangeText = (key, value) => {
    setState(prevState => ({
      ...prevState,
      [key]: value,
    }));
  }  

  const submit = async () => {
    setLoading(true);
    setErrors({});
    try {
      // const response = await registerWithOtp({ ...state, role_id }).unwrap();
      // console.log(response);
      // if (response.success) {
      //   navigation.navigate('VerifyContactNoSendOTP', {
      //     sms_token: response.sms_token,
      //     registration: { ...state, role_id },
      //   });
      // } else {
      //   dropdownAlert.current.alertWithType('error', response.message || 'Error');
      // }
      // setLoading(false);
    } catch (error) {
      console.log(error);
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      setLoading(false);
    }
  }

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={{
        flexGrow: 1,
      }}
      keyboardShouldPersistTaps="always"
    >
      <Box
        style={{
          flex: 1,
        }}
      >
        <Box
          style={{
            width: '100%',
          }}
        >
          <VStack 
            space="5"
            w="100%" 
            p="5"
          >
            <FormControl isInvalid={hasError('first_name', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                autoCapitalize="none"
                autoCorrect={false}
                value={state.first_name}
                placeholder="First Name"
                onChangeText={text => onChangeText('first_name', text)}
              />
              {hasError('first_name', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('first_name', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('last_name', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                autoCapitalize="none"
                autoCorrect={false}
                value={state.last_name}
                placeholder="Last Name"
                onChangeText={text => onChangeText('last_name', text)}
              />
              {hasError('last_name', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('last_name', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('gender', errors)}>
              <Select 
                bg="#eeeeee"
                color="#000000"
                selectedValue={state.gender}
                placeholder="Choose Gender" 
                onValueChange={text => onChangeText('gender', text)}
              >
                <Select.Item label="Male" value="Male" />
                <Select.Item label="Female" value="Female" />
              </Select>
              {hasError('gender', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('gender', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('date_of_birth', errors)}>
              <DatePicker
                value={state.date_of_birth}
                onDateTimeChange={text => onChangeText('date_of_birth', text)}
              />
              {hasError('date_of_birth', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('date_of_birth', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('address', errors)}>
              <TextArea
                bg="#eeeeee"
                color="#000000"
                value={state.address}
                placeholder="Address"
                onChangeText={text => onChangeText('address', text)}
              />
              {hasError('address', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('address', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('email', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                keyboardType="email-address"
                value={state.email}
                placeholder="Email"
                onChangeText={text => onChangeText('email', text)}
              />
              {hasError('email', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('email', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('password', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                type="password"
                value={state.password}
                placeholder="Password"
                onChangeText={text => onChangeText('password', text)}
              />
              {hasError('password', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('password', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('password_confirmation', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                type="password"
                placeholder="Confirm Password"
                value={state.password_confirmation}
                onChangeText={text => onChangeText('password_confirmation', text)}
              />
              {hasError('password_confirmation', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('password_confirmation', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <Button
              size="lg"
              w="100%"
              bg="#dc143b"
              _text={{ color: '#ffffff'}}
              onPress={submit}
              isLoading={loading}
            >
              Register
            </Button>
          </VStack>
        </Box>
      </Box>
    </KeyboardAwareScrollView>
  );
}

export default Register;