export const icons = {
  16: require('../../assets/icons/16.png'),
  15: require('../../assets/icons/15.png'),
  14: require('../../assets/icons/14.png'),
  13: require('../../assets/icons/13.png'),
  12: require('../../assets/icons/12.png'),
  11: require('../../assets/icons/11.png'),
  7: require('../../assets/icons/7.png'),
  6: require('../../assets/icons/6.png'),
  5: require('../../assets/icons/5.png'),
  4: require('../../assets/icons/4.png'),
};

export const iconsByType = {
  'Police': require('../../assets/icons/7.png'),
  'Medical': require('../../assets/icons/6.png'),
  'Disaster': require('../../assets/icons/5.png'),
  'Fire': require('../../assets/icons/4.png'),
};