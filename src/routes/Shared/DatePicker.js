import React, { useState, useEffect } from 'react';
import { View, Platform, TouchableOpacity } from 'react-native';
import { Box, Input, Text, HStack } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import RNDateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';

function DatePicker({
  value,
  onDateTimeChange,
  style
}) {
  const [date, setDate] = useState(value ? new Date(value) : new Date());
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setShow(false);
    setDate(currentDate);
  };

  const handleConfirm = currentDate => {
    setShow(false);
    setDate(currentDate);
  };

  useEffect(() => {
    if (date) {
      onDateTimeChange(moment(date).format('YYYY-MM-DD'));
    }
  }, [date]);

  return (
    <View style={style}>
      <TouchableOpacity
        onPress={() => setShow(true)} 
      >
        <Input
          InputRightElement={<Icon name="calendar" size={30} color="#000000" />}
          bg="#eeeeee"
          color="#000000"
          placeholder="Select Date"
          value={date ? moment(date).format('MMMM DD, YYYY') : 'Select Date'}
          onChangeText={text => {}}
          isReadOnly={true}
        />
      </TouchableOpacity>
      {Platform.OS === 'ios' ? (
        <>
          <RNDateTimePickerModal
            isVisible={show}
            value={date}
            mode="date"
            onConfirm={handleConfirm}
            onCancel={() => setShow(false)}
          />
        </>
      ) : (
        <>
          {show && (
            <RNDateTimePicker
              value={date}
              mode="date"
              is24Hour={false}
              onChange={onChange}
            />
          )}
        </>
      )}
    </View>
  )
}

export default DatePicker;
