import React, { useState, useEffect } from 'react'; 
import { View, Image, StyleSheet, Dimensions } from 'react-native';
import Loader from './Loader';

function FullWidthImage({ source, ...props }) {
  const [loading, setLoading] = useState(true);
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);

  useEffect(() => {
    Image.getSize(source.uri, (width, height) => {
      // calculate image width and height 
      const screenWidth = Dimensions.get('window').width;
      const scaleFactor = width / screenWidth;
      const imageHeight = height / scaleFactor;

      setWidth(screenWidth);
      setHeight(imageHeight);
      setLoading(false);
    })
  }, []);

  return loading ? <Loader /> : (
    <Image
      style={{ width, height }}
      source={source}
      alt={source.uri}
      {...props}
    />
  );
}

export default FullWidthImage;