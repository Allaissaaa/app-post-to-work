import React from 'react';
import { Box, Center, Image, Text } from 'native-base';
import * as Animatable from 'react-native-animatable';

function Loader() {
  return (
    <Center bg="white" flex="1" w="100%" h="100%">
      <Box p="3">
        <Animatable.View 
          animation="pulse" 
          easing="ease-out"
          iterationCount="infinite"
          direction="alternate"
        >          
          <Image
            source={require('../../assets/logo.png')}
            style={{ 
              width: 200, 
              height: 200,
              marginBottom: 30,
            }}
          />
        </Animatable.View>
      </Box>
      <Text>Please Wait...</Text>
    </Center>
  )
}

export default Loader;