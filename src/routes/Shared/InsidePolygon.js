import React, { useEffect, useState } from 'react';
import { View, ActivityIndicator, Dimensions } from 'react-native';
import { useDisclose, Actionsheet, Pressable, Heading, WarningOutlineIcon, Box, VStack, Image, Text, Button, FormControl, Input, Link, Center, HStack } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useSelector } from 'react-redux';
import { hasError } from '../../helpers/utils';
import { useLoginMutation } from '../../redux/services/login';
import Geolocation from '@react-native-community/geolocation';
import GeoFencing from 'react-native-geo-fencing';
import coordinates from '../../assets/coordinates.json';

const { width } = Dimensions.get('window');

function InsidePolygon({ children }) {
  const [is_inside_polygon, setIsInsidePolygon] = useState(true);

  const polygon = coordinates[0].geojson.coordinates[0].map(coordinate => ({
    latitude: coordinate[1],
    longitude: coordinate[0],
  }));

  useEffect(() => {
    // Geolocation.getCurrentPosition(position => {
    //   let point = {
    //     lat: position.coords.latitude,
    //     lng: position.coords.longitude
    //   };

    //   GeoFencing.containsLocation(point, polygon)
    //     .then(() => setIsInsidePolygon(true))
    //     .catch(() => setIsInsidePolygon(false))
    //   }, error => console.log(error.message), 
    //   { enableHighAccuracy: false, timeout: 15000, maximumAge: 0 },
    // );
  }, []);

  return is_inside_polygon ? children : (
    <KeyboardAwareScrollView
      contentContainerStyle={{
        flexGrow: 1,
        backgroundColor: '#ffffff',
      }}
      keyboardShouldPersistTaps="always"
    >
      <Box
        style={{
          flex: 1,
        }}
      >
        <Center
          style={{
            flex: 1,
          }}
          p="10"
        >
          <Image
            source={require('../../assets/logo.png')}
            style={{ 
              width: 200, 
              height: 200,
            }}
            PlaceholderContent={<ActivityIndicator />}
            alt="logo"
          />
          <Heading size="sm" mb="20" textAlign="center">Habal Habal Booking System</Heading>
          <Heading mb="20" textAlign="center">
            Sorry! this app is for Cotabato City Only.
          </Heading>
        </Center>
      </Box>
    </KeyboardAwareScrollView>
  );
}

export default InsidePolygon;