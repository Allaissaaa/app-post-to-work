import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { Text } from 'native-base';

class Clock extends Component {

  static defaultProps = {
    onTimeEnd: () => {},
    customUi: null,
    customUiWithLeadingZero: null,
    daysVisibility: true,
    hoursVisibility: true,
    minutesVisibility: true,
    secondsVisibility: true,
    timeExpiredText: 'Time Expired',
    clockTextStyle: {},
  };

  static propTypes = {
    deadline: PropTypes.any.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      deadline: null,
      days: 0,
      hours: 0,
      minutes: 0,
      seconds: 0,
      time_expired: false,
    };
  }

  componentDidMount() {
    this.startTimer();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  startTimer() {
    const { deadline } = this.props;
    this.setState({ deadline });
    clearInterval(this.interval);
    this.interval = setInterval(() => this.getTimeUntil(deadline), 1000);
  }

  leadingZero(num) {
    return num < 10 ? '0' + num : num;
  }

  getTimeUntil(deadline) {
    const { onTimeEnd } = this.props;
    const time = Date.parse(deadline) - Date.parse(new Date());
    if (time < 0) {
      clearInterval(this.interval);
      this.setState({
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
        time_expired: true,
      });
      onTimeEnd();
    } else {
      const seconds = Math.floor((time / 1000) % 60);
      const minutes = Math.floor((time / 1000 / 60) % 60);
      const hours = Math.floor((time / (1000 * 60 * 60)) % 24);
      const days = Math.floor(time / (1000 * 60 * 60 * 24));
      this.setState({
        days,
        hours,
        minutes,
        seconds,
      });
    }
  }

  render() {
    const {
      customUi,
      customUiWithLeadingZero,
      daysVisibility,
      hoursVisibility,
      minutesVisibility,
      secondsVisibility,
      timeExpiredText,
      clockTextStyle,
    } = this.props;
    const {
      days,
      hours,
      minutes,
      seconds,
      time_expired,
    } = this.state;
    const _days = this.leadingZero(this.state.days);
    const _hours = this.leadingZero(this.state.hours);
    const _minutes = this.leadingZero(this.state.minutes);
    const _seconds = this.leadingZero(this.state.seconds);

    if (time_expired) {
      return timeExpiredText ? (
        <View
          style={{
            flex:1,
            flexDirection: 'row',
          }}>
          <Text>{timeExpiredText}</Text>
        </View>
      ) : null;
    }

    if (customUi) {return customUi(days, hours, minutes, seconds);}
    if (customUiWithLeadingZero) {return customUiWithLeadingZero(_days, _hours, _minutes, _seconds);}

    return (
      <Text style={[clockTextStyle]}>
        {daysVisibility && `${_days} Days `}
        {hoursVisibility && `${_hours} Hours `}
        {minutesVisibility && `${_minutes} Minutes `}
        {secondsVisibility && `${_seconds} Seconds `}
      </Text>
    );
  }
}

export default Clock;
