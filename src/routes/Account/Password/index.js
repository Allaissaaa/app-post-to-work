import React, { useState, useCallback, useMemo, useRef } from 'react';
import { View, ActivityIndicator, useWindowDimensions } from 'react-native';
import { Pressable, Heading, WarningOutlineIcon, Box, VStack, Image, Text, Button, FormControl, Input, Link, Center, HStack } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch } from 'react-redux';
import { storeUser } from '../../../redux/reducers/user';
import { storeToken } from '../../../redux/reducers/token';
import { updateState } from '../../../helpers/updateState';
import { useDropDown } from '../../../context/DropDownComponentProvider';
import { hasError } from '../../../helpers/utils';
import { useUpdatePasswordMutation } from '../../../redux/services/profile';
import LinearGradient from 'react-native-linear-gradient';

function Password({ navigation }) {
  const { width } = useWindowDimensions();
  const dispatch = useDispatch();
  const { dropdownAlert } = useDropDown();
  const [updatePassword] = useUpdatePasswordMutation();
  const [state, setState] = useState({
    password: '',
    password_confirmation: '',
  });
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});

  const onChangeText = (key, value) => {
    setState(prevState => ({
      ...prevState,
      [key]: value,
    }));
  }  

  const submit = async () => {
    setErrors({});
    setLoading(true);
    try {
      const response = await updatePassword({
        ...state
      }).unwrap();
      setState({
        password: '',
        password_confirmation: '',
      });
      dropdownAlert.current.alertWithType('success', 'Your password has been updated.');
      setLoading(false);
    } catch (error) {
      console.log(error);
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      setLoading(false);
    }
  };

  return (
    <LinearGradient colors={['#ffffff', '#7792f4']} style={{ flex: 1 }}>
      <KeyboardAwareScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}
        keyboardShouldPersistTaps="always"
      >
        <VStack 
          space="5"
          w="100%" 
          p="5"
        >
          <FormControl>
            <Input
              bg="#eeeeee"
              color="#000000"
              type="password"
              placeholder="Password"
              value={state.password}
              onChangeText={text => onChangeText('password', text)}
            />
          </FormControl>
          <FormControl isInvalid={hasError('password_confirmation', errors)}>
            <Input
              bg="#eeeeee"
              color="#000000"
              type="password"
              placeholder="Retype Password"
              value={state.password_confirmation}
              onChangeText={text => onChangeText('password_confirmation', text)}
            />
            {hasError('password_confirmation', errors) && (
            <FormControl.ErrorMessage
              leftIcon={<WarningOutlineIcon size={5} />}
            >
              {hasError('password_confirmation', errors)}
            </FormControl.ErrorMessage>
            )}
          </FormControl>
          <Button
            size="lg"
            w="100%"
            bg="#dc143b"
            _text={{ color: '#ffffff'}}
            onPress={submit}
            isLoading={loading}
          >
            Update
          </Button>
        </VStack>
      </KeyboardAwareScrollView>
    </LinearGradient>
  );
}

export default Password;