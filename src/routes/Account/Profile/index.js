import React, { useState, useEffect } from 'react';
import { ScrollView, useWindowDimensions } from 'react-native';
import { useDispatch } from 'react-redux';
import { Select, useToast, Box, Text, HStack, Avatar, VStack, Center, FormControl, Input, Radio, Button, WarningOutlineIcon, TextArea, Heading } from 'native-base';
import { useAuth } from '../../../helpers/auth';
import { storeUser, updateUserFields } from '../../../redux/reducers/user';
import { updateState } from '../../../helpers/updateState';
import { useUpdateProfileMutation, useUpdateAvatarMutation } from '../../../redux/services/profile';
import { useUploadMutation } from '../../../redux/services/files';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';
import SingleFilePicker from '../../../hooks/SingleFilePicker';
import { hasError } from '../../../helpers/utils';
import { useDropDown } from '../../../context/DropDownComponentProvider';
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from '../../Shared/DatePicker';

function Edit({ navigation }) {
  const { width } = useWindowDimensions();
  const dispatch = useDispatch();
  const auth = useAuth();
  const toast = useToast();
  const { dropdownAlert } = useDropDown();
  const [updateAvatar] = useUpdateAvatarMutation();
  const [fileUploader] = useUploadMutation();
  const [updateProfile] = useUpdateProfileMutation();
  const [state, setUser] = useState({
    first_name: auth.getUser?.first_name || '',
    middle_name: auth.getUser?.middle_name || '',
    last_name: auth.getUser?.last_name || '',
    address: auth.getUser?.address || '',
    contact_no: auth.getUser?.contact_no || '',
    gender: auth?.getUser?.gender || '',
    date_of_birth: auth?.getUser?.date_of_birth || '',
  });
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});

  const onChangeText = (key, value) => {
    setUser(prevState => ({
      ...prevState,
      [key]: value,
    }));
  }

  const submit = async () => {
    setErrors({});
    setLoading(true);
    try {
      const response = await updateProfile(state).unwrap();
      await dispatch(updateState(storeUser(response.user)));
      dropdownAlert.current.alertWithType('success', 'Your profile has been updated.');
      setLoading(false);
    } catch (error) {
      console.log(error);
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      setLoading(false);
    }
  };

  const avatarUpdate = async file => {
    try {
      const data = new FormData();
      data.append('file', file);
      const response = await fileUploader(data).unwrap();
      const response1 = await updateAvatar({ photo: response.data }).unwrap();
      console.log(response, response1);
      await dispatch(updateState(storeUser(response1.user)));
    } catch(error) {
      console.log(error);
    }
  }
  
  return (
    <LinearGradient colors={['#ffffff', '#7792f4']} style={{ flex: 1 }}>
      <KeyboardAwareScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}
        keyboardShouldPersistTaps="always"
      >
        <Box
          style={{
            flex: 1,
          }}
        >
          <Center pb="5" pt="5">
            <SingleFilePicker
              onFileSelect={avatarUpdate}
            >
              <Avatar
                key={auth?.getAvatar}
                bg="grey.600"
                size="2xl"
                source={{
                  uri: auth.getAvatar,
                }}
                alt={auth.getName}
              >
                USER
                <Avatar.Badge bg="transparent" width={30} height={30}><Icon name="plus" size={30} color="#aeaeae" /></Avatar.Badge>
              </Avatar>
            </SingleFilePicker>
          </Center>
          <VStack 
            space="5"
            w="100%" 
            p="5"
          >
            <FormControl isInvalid={hasError('first_name', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                autoCapitalize="none"
                autoCorrect={false}
                value={state.first_name}
                placeholder="First Name"
                onChangeText={text => onChangeText('first_name', text)}
              />
              {hasError('first_name', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('first_name', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('middle_name', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                autoCapitalize="none"
                autoCorrect={false}
                value={state.middle_name}
                placeholder="Middle Name"
                onChangeText={text => onChangeText('middle_name', text)}
              />
              {hasError('middle_name', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('middle_name', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('last_name', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                autoCapitalize="none"
                autoCorrect={false}
                value={state.last_name}
                placeholder="Last Name"
                onChangeText={text => onChangeText('last_name', text)}
              />
              {hasError('last_name', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('last_name', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('contact_no', errors)}>
              <Input
                bg="#eeeeee"
                color="#000000"
                value={state.contact_no}
                placeholder="Contact No."
                onChangeText={text => onChangeText('contact_no', text)}
              />
              {hasError('contact_no', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('contact_no', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('gender', errors)}>
              <Select 
                bg="#eeeeee"
                color="#000000"
                selectedValue={state.gender}
                placeholder="Choose Gender" 
                onValueChange={text => onChangeText('gender', text)}
              >
                <Select.Item label="Male" value="Male" />
                <Select.Item label="Female" value="Female" />
              </Select>
              {hasError('gender', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('gender', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('date_of_birth', errors)}>
              <DatePicker
                value={state.date_of_birth}
                onDateTimeChange={text => onChangeText('date_of_birth', text)}
              />
              {hasError('date_of_birth', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('date_of_birth', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <FormControl isInvalid={hasError('address', errors)}>
              <TextArea
                bg="#eeeeee"
                color="#000000"
                value={state.address}
                placeholder="Address"
                onChangeText={text => onChangeText('address', text)}
              />
              {hasError('address', errors) && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {hasError('address', errors)}
              </FormControl.ErrorMessage>
              )}
            </FormControl>
            <Button
              size="lg"
              w="100%"
              bg="#dc143b"
              _text={{ color: '#ffffff'}}
              onPress={submit}
              isLoading={loading}
            >
              Update
            </Button>
          </VStack>
        </Box>
      </KeyboardAwareScrollView>
    </LinearGradient>
  );
}

export default Edit;