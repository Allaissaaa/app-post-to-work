import React, { useState, useCallback, useMemo, useRef } from 'react';
import { View, ActivityIndicator, useWindowDimensions } from 'react-native';
import { useDisclose, Actionsheet, Pressable, Heading, WarningOutlineIcon, Box, VStack, Image, Text, Button, FormControl, Input, Link, Center, HStack } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useSelector } from 'react-redux';
import { hasError } from '../../helpers/utils';
import { useLoginMutation } from '../../redux/services/login';

function LandingPage({ navigation }) {
  const { width } = useWindowDimensions();
  const { isOpen, onOpen, onClose } = useDisclose();

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={{
        flexGrow: 1,
      }}
      keyboardShouldPersistTaps="always"
    >
      <Box
        style={{
          flex: 1,
        }}
      >
        <Center
          style={{
            flex: 1,
          }}
          p="5"
        >
          <Image
            source={require('../../assets/logo.png')}
            style={{ 
              width: 200, 
              height: 200,
            }}
            PlaceholderContent={<ActivityIndicator />}
            alt="logo"
            mb="6"
          />
          <HStack space={4} alignItems="center">
            <Button
              w={width / 3}
              bg="#008080"
              _text={{ color: '#ffffff'}}
              onPress={() => onOpen()}
            >
              Register Now
            </Button>
            <Button
              w={width / 3}
              bg="#000000"
              _text={{ color: '#ffffff'}}
              onPress={() => navigation.navigate('LogIn')}
            >
              Login
            </Button>
          </HStack>
        </Center>
      </Box>
      <Actionsheet isOpen={isOpen} onClose={onClose} disableOverlay>
        <Actionsheet.Content>
          <Box w="100%" h={60} px={4} justifyContent="center">
            <Text 
              fontSize="16" 
              color="gray.500" 
              _dark={{
                color: "gray.300"
              }}
            >
              Are you a:
            </Text>
          </Box>
          <Actionsheet.Item 
            onPress={() => {
              onClose();
              navigation.navigate('Register', { role_id: 2 });
            }}
          >
            Client
          </Actionsheet.Item>
          <Actionsheet.Item 
            onPress={() => {
              onClose();
              navigation.navigate('Register', { role_id: 3 });
            }}
          >
            Freelancer
          </Actionsheet.Item>
        </Actionsheet.Content>
      </Actionsheet>
    </KeyboardAwareScrollView>
  );
}

export default LandingPage;