import React, { useState, useCallback, useEffect, useRef } from 'react';
import { View, ActivityIndicator, Dimensions } from 'react-native';
import { Alert, HStack, Pressable, Heading, WarningOutlineIcon, Box, VStack, Image, Text, Button, FormControl, Input, Link, Center, InputGroup, InputLeftAddon } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch } from 'react-redux';
import { storeUser } from '../../redux/reducers/user';
import { storeToken } from '../../redux/reducers/token';
import { updateState } from '../../helpers/updateState';
import { useDropDown } from '../../context/DropDownComponentProvider';
import { hasError } from '../../helpers/utils';
import { useVerifyContactNoSendOtpMutation, useVerifyContactNoMutation } from '../../redux/services/register';
import { useAuth } from '../../helpers/auth';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import LinearGradient from 'react-native-linear-gradient';

const { width } = Dimensions.get('window');

function VerifyContactNoSendOTP({ route, navigation }) {
  const { sms_token, registration } = route.params || {};
  const dispatch = useDispatch();
  const { dropdownAlert } = useDropDown();
  const [sendVerificationCode] = useVerifyContactNoSendOtpMutation();
  const [verifyContactNo] = useVerifyContactNoMutation();
  const [state, setState] = useState({
    contact_no: registration.contact_no,
  });
  const [code, setCode] = useState('');
  const [token, setToken] = useState('');
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState({});

  console.log(sms_token, registration);

  useEffect(() => {
    if (sms_token) {
      setToken(sms_token);
    }
  }, [sms_token]);

  const onChangeText = (key, value) => {
    setState(prevState => ({
      ...prevState,
      [key]: value,
    }));
  }

  const submit = async () => {
    setLoading(true);
    setErrors({});
    try {
      const response = await verifyContactNo({ ...registration, code, token }).unwrap();
      if (response.success) {
        await dispatch(updateState(storeUser(response.user)));
        await dispatch(updateState(storeToken(response.token)));

        dropdownAlert.current.alertWithType('success', response.message);
      } else {
        dropdownAlert.current.alertWithType('error', '');
      }
      setLoading(false);
    } catch (error) {
      console.log(error);
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      setLoading(false);
    }
  };

  const sendOTP = async () => {
    setLoading(true);
    setErrors({});
    try {
      const response = await sendVerificationCode(state).unwrap();
      console.log(response);
      if (response.success) {
        setToken(response.token);

        dropdownAlert.current.alertWithType('success', response.message);
      } else {
        dropdownAlert.current.alertWithType('error', '');
      }
      setLoading(false);
    } catch (error) {
      console.log(error);
      if (error.status === 422) {
        setErrors(error.data.errors);
      }
      setLoading(false);
    }
  };

  return (
    <LinearGradient colors={['#ffffff', '#7792f4']} style={{ flex: 1 }}>
      <KeyboardAwareScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}
        keyboardShouldPersistTaps="always"
      >
        <Box
          style={{
            flex: 1,
          }}
        >
          <Center
            mt="10"
          >
            <Heading size="2xl">Verify your</Heading>
            <Heading size="2xl" mb="3">phone number</Heading>
          </Center>
          <Box
            style={{
              width: '100%',
            }}
          >
            <VStack 
              space="5"
              w="100%" 
              p="5"
            >
              <FormControl isInvalid={hasError('contact_no', errors)} mb="10">
                <HStack
                  width="100%"
                  alignItems="center"      
                >
                  <Box>
                    <Image
                      mr="3"
                      source={require('../../assets/ph.png')}
                      style={{ 
                        width: 30, 
                        height: 17,
                      }}
                      PlaceholderContent={<ActivityIndicator />}
                      alt="logo"
                    />
                  </Box>
                  <Box flex={1}>
                    <Input
                      InputLeftElement={<Box pl="2"><Text>(+63)</Text></Box>}
                      bg="#eeeeee"
                      color="#000000"
                      keyboardType="numeric"
                      placeholder="Enter Phone Number"
                      value={state.contact_no}
                      onChangeText={text => onChangeText('contact_no', text)}
                      isDisabled={token ? true : false}
                      maxLength={10}
                    />
                  </Box>
                </HStack>
                {hasError('contact_no', errors) && (
                <FormControl.ErrorMessage
                  leftIcon={<WarningOutlineIcon size="xs" />}
                >
                  {hasError('contact_no', errors)}
                </FormControl.ErrorMessage>
                )}
              </FormControl>
              {token ? (
              <FormControl isInvalid={hasError('code', errors)}>
                <Input
                  bg="#eeeeee"
                  color="#000000"
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={code}
                  placeholder="Code"
                  onChangeText={text => setCode(text)}
                />
                {hasError('code', errors) && (
                <FormControl.ErrorMessage
                  leftIcon={<WarningOutlineIcon size="xs" />}
                >
                  {hasError('code', errors)}
                </FormControl.ErrorMessage>
                )}
              </FormControl>
              ) : null}
              <Text mb="10" color="#ffffff">To make sure this number is yours, Google will send you a text message with a 6-digit verification contact no.</Text>
              <Center>
                {token ? (
                <Button
                  p="5"
                  borderRadius="50"
                  w={width - (width / 3)}
                  bg="#3f8acc"
                  _text={{ color: '#ffffff'}}
                  onPress={submit}
                  isLoading={loading}
                >
                  verify
                </Button>
                ) : (
                <Button
                  p="5"
                  borderRadius="50"
                  w={width - (width / 3)}
                  bg="#3f8acc"
                  _text={{ color: '#ffffff'}}
                  onPress={sendOTP}
                  isLoading={loading}
                >
                  send otp
                </Button>
                )}
              </Center>
            </VStack>
            {token ? (
              <Center>
                <Button
                  variant="ghost"
                  borderRadius="50"
                  _text={{ color: '#ffffff'}}
                  w={200}
                  onPress={sendOTP}
                >
                  resend verification code
                </Button>
              </Center>
            ) : null}
          </Box>
        </Box>
      </KeyboardAwareScrollView>
    </LinearGradient>
  );
}

export default VerifyContactNoSendOTP;