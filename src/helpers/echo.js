import { useEffect } from 'react-native';
import LaravelEcho from 'laravel-echo';
import Pusher from 'pusher-js/react-native';
import { store } from '../configureStore';
import { PUSHER_KEY, PUSHER_AUTH_ENDPOINT, WEBSOCKET_HOST, WEBSOCKET_PORT } from '../config';
import Toast from 'react-native-simple-toast';

const token = store.getState()?.token?.value || '';

const Echo = new LaravelEcho({
  broadcaster: 'pusher',
  client: new Pusher(PUSHER_KEY, {
    key: PUSHER_KEY,
    wsHost: WEBSOCKET_HOST,
    wsPort: WEBSOCKET_PORT,
    wssPort: WEBSOCKET_PORT,
    enabledTransports: ['ws', 'wss'],
    auth: {
      headers: {
        'Authorization': 'Bearer ' + token,
        Accept: "application/json"
      }
    },
    authEndpoint: PUSHER_AUTH_ENDPOINT,
    cluster: 'eu',
  }),
});

Echo.connector.pusher.connection.bind('connecting', (payload) => {
  console.log('connecting...');

  Toast.showWithGravity('Connecting...', Toast.SHORT, Toast.TOP);
});

Echo.connector.pusher.connection.bind('connected', (payload) => {
  console.log('connected!', payload);
  Toast.showWithGravity('Connected.', Toast.SHORT, Toast.TOP);
});

Echo.connector.pusher.connection.bind('unavailable', (payload) => {
  console.log('unavailable', payload);
  Toast.showWithGravity('Unavailable', Toast.SHORT, Toast.TOP);
});

Echo.connector.pusher.connection.bind('failed', (payload) => {
  console.log('failed', payload);
  Toast.showWithGravity('Failed', Toast.SHORT, Toast.TOP);
});

Echo.connector.pusher.connection.bind('disconnected', (payload) => {
  console.log('disconnected', payload);
  Toast.showWithGravity('Disconnected', Toast.SHORT, Toast.TOP);
});

Echo.connector.pusher.connection.bind('message', (payload) => {
  console.log('message', payload);
});

export default Echo;