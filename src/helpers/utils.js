import he from 'he';
import moment from 'moment';
import { decode } from 'html-entities';

import Geolocation from '@react-native-community/geolocation';
import Geocoder from '@timwangdev/react-native-geocoder';
import { request, PERMISSIONS } from 'react-native-permissions';

export const getCurrentLocation = async callback => {
  await request(Platform.select({
    android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
  }));

  return Geolocation.getCurrentPosition(position => {
      Geocoder.geocodePosition({
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      }).then(response => callback(response[0].formattedAddress, position));
    }, error => console.log(error.message), 
    { enableHighAccuracy: true, timeout: 60000 },
  );
};

export function regionFrom(lat, lon, accuracy) {
  const oneDegreeOfLongitudeInMeters = 111.32 * 1000;
  const circumference = (40075 / 360) * 1000;

  const latDelta = accuracy * (1 / (Math.cos(lat) * circumference));
  const lonDelta = (accuracy / oneDegreeOfLongitudeInMeters);

  return {
    latitude: lat,
    longitude: lon,
    latitudeDelta: Math.max(0, latDelta),
    longitudeDelta: Math.max(0, lonDelta)
  };
} 

export function getLatLonDiffInMeters(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d * 1000;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

export const statusColor = status => {
  if (['Declined', 'Not Available'].includes(status)) {
    return 'danger';
  }
  if (['Pending'].includes(status)) {
    return 'info';
  }

  return 'success';
};

export const getPercentage = (number, percent) => {
  return (percent / 100) * number;
};

export const htmlEntities = returnText => {
  //-- remove BR tags and replace them with line break
  returnText = returnText.replace(/<br>/gi, "\n");
  returnText = returnText.replace(/<br\s\/>/gi, "\n");
  returnText = returnText.replace(/<br\/>/gi, "\n");

  // //-- remove P and A tags but preserve what's inside of them
  returnText = returnText.replace(/<p.*>/gi, "\n");
  returnText = returnText.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 ($1)");

  // //-- remove all inside SCRIPT and STYLE tags
  returnText = returnText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
  returnText = returnText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");

  // //-- remove all else
  returnText = returnText.replace(/<(?:.|\s)*?>/g, "");

  // //-- get rid of more than 2 multiple line breaks:
  returnText = returnText.replace(/(?:(?:\r\n|\r|\n)\s*){2,}/gim, "");
  // returnText = returnText.replace(/(?:(?:\r\n|\r|\n)\s*){2,}/gim, "\n\n");

  //-- get rid of more than 2 spaces:
  returnText = returnText.replace(/ +(?= )/g, '');

  returnText = returnText.replace(/<[^>]+>/g, '').replace(/&lt;/g, "<").replace(/&gt;/g, ">")
  returnText = decode(returnText);
  
  return returnText;
};

export const timeAgo = time => {
  moment.updateLocale('en', {
    relativeTime: {
      future: "in %s",
      past: "%s ago",
      s: number=>number + "s ago",
      ss: '%ds ago',
      m: "1m ago",
      mm: "%dm ago",
      h: "1h ago",
      hh: "%dh ago",
      d: "1d ago",
      dd: "%dd ago",
      M: "a month ago",
      MM: "%d months ago",
      y: "a year ago",
      yy: "%d years ago"
    }
  });

  let secondsElapsed = moment().diff(time, 'seconds');
  let dayStart = moment("2018-01-01").startOf('day').seconds(secondsElapsed);

  if (secondsElapsed > 300) {
    return moment(time).fromNow(true);
  } else if (secondsElapsed < 60) {
    return dayStart.format('s') + 's ago';
  } else {
    return dayStart.format('m:ss') + 'm ago';
  }
}

export const truncateString = (string = '', maxLength = 50) => 
  string.length > maxLength 
    ? `${string.substring(0, maxLength)}…`
    : string

export const getInitials = (name) => {
  let initials = name.split(' ');
  
  if(initials.length > 1) {
    initials = initials.shift().charAt(0) + initials.pop().charAt(0);
  } else {
    initials = name.substring(0, 2);
  }
  
  return initials.toUpperCase();
}

export const formatString = (array = []) => {
 return array.reduce((acc, item, index, arr) => {
    let separator = '';
  
    if (index !== 0) {
      separator = index === arr.length - 1 ? ' and ' : ', ';
    }
 
    return acc+separator+item;
  }, '');
};

export const toTitleCase = (str = '') => {
  return str.replace(
    /\w\S*/g,
    function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
}

export const hasError = (field_name = '', errors = {}) => {
  if (!errors) return false;
  if (!errors.hasOwnProperty(field_name)) return false;

  return errors[field_name].map(error => he.decode(error).trim()).join(', \n');
};