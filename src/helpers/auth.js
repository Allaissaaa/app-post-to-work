import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectUser } from '../redux/reducers/user';
import { selectToken } from '../redux/reducers/token';
import { getInitials } from '../helpers/utils';

export const useAuth = () => {
  const dispatch = useDispatch();
  const token = useSelector(selectToken);
  const user = useSelector(selectUser);

  return {
    isAuthenticated: (token && user) ? true : false,
    isEmailVerified: user?.email_verified_at ? true : false,
    isContactNoVerified: user?.contact_no_verified_at ? true : false,
    isProfileCompleted: user?.is_profile_completed,
    isClient: user?.role_id === 2,
    isFreelancer: user?.role_id === 3,
    getId: user?.id,
    getName: `${user?.first_name}${user?.middle_name ? (` ${user?.middle_name} `) : ' '}${user?.last_name}`,
    getFirstName: user?.first_name,
    getLastName: user?.last_name,
    getAvatar: user?.photo,
    getEmail: user?.email,
    getContactNo: user?.contact_no,
    getRole: { 2: 'Client', 3: 'Freelancer'  }[user?.role_id],
    getRoleId: user?.role_id,
    getToken: token,
    getUser: user,
    getStatus: user?.status,
    logout: (callback = null) => {
      dispatch({ type: 'USER_LOGOUT' });
      if (callback) {
        callback();
      }
    }
  }
}