import 'react-native-gesture-handler';
import * as React from 'react';
import { LogBox } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { NativeBaseProvider } from 'native-base';
import OneSignal from 'react-native-onesignal';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { DropDownComponentProvider } from './src/context';
import { store, persistor } from './src/configureStore';
import Router from './src/router';

import * as Sentry from '@sentry/react-native';

Sentry.init({ 
  dsn: 'https://2668640a7456d006be7b596c559c24f7@o4505803998035968.ingest.sentry.io/4505804697174016', 
});

navigator.geolocation = require('@react-native-community/geolocation');

LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreLogs(['VirtualizedLists should never']);
LogBox.ignoreAllLogs();

OneSignal.setLogLevel(6, 0);
OneSignal.setAppId('d55b6d09-8e70-429b-8c32-f483a009e743');

function App() {
  return (
    <SafeAreaProvider>
      <NativeBaseProvider>
        <DropDownComponentProvider>
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <Router />
            </PersistGate>
          </Provider>
        </DropDownComponentProvider>
      </NativeBaseProvider>
    </SafeAreaProvider>
  );
}

export default App;